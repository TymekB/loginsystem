<?php

    require "backend/User.php";

    $user = new User();

    if($user->isLogged())
    {
        header('Location: panel.php');
        exit();
    }

    if(isset($_POST['username']))
    {
        $validation_ok = true;

        $username = htmlentities($_POST['username'], ENT_QUOTES);

        // Username validation

        if(strlen($username) < 3)
        {
            $_SESSION['e_username'] = "Username is too short! (3-20)";
            $validation_ok = false;
        }

        if(strlen($username) > 20)
        {
            $_SESSION['e_username'] = "Username is too long! (3-20)";
            $validation_ok = false;
        }

        if(!ctype_alnum($username))
        {
            $_SESSION['e_username'] = "Username is invalid";
            $validation_ok = false;
        }

        if($user->searchUsername($username))
        {
            $_SESSION['e_username'] = "Username already exist!";
            $validation_ok = false;
        }

        // Email validation

        $email = $_POST['email'];
        $sanitizedEmail = filter_var($email, FILTER_SANITIZE_EMAIL);

        if(!filter_var($sanitizedEmail, FILTER_VALIDATE_EMAIL) || $sanitizedEmail != $email)
        {
            $_SESSION['e_email'] = "Email is invalid!";
            $validation_ok = false;
        }

        if($user->searchEmail($email))
        {
            $_SESSION['e_email'] = "Email already exist!";
            $validation_ok = false;
        }

        // Password validation

        $password1 = $_POST['password1'];
        $password2 = $_POST['password2'];

        if(strlen($password1) < 8)
        {
            $_SESSION['e_pass'] = "Password is too short! (8-20)";
            $validation_ok = false;
        }

        if(strlen($password1) > 20)
        {
            $_SESSION['e_pass'] = "Password is too long! (8-20)";
            $validation_ok = false;
        }

        if($password1 != $password2)
        {
            $_SESSION['e_pass'] = "Passwords are not the same!";
            $validation_ok = false;
        }

        // Checkbox validation

        if(!isset($_POST['terms-of-service']))
        {
            $_SESSION['e_checkbox'] = "Confirm terms of service!";
        }

        // ReCAPTCHA validation

        $secret = "6Lc2VCcUAAAAAEh5drpBUSoBXyhiykhbgeHMiML7";

        $response = json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secret."&response=".$_POST['g-recaptcha-response']));

        if(!$response->success)
        {
            $_SESSION['e_recaptcha'] = "reCAPTCHA must be selected!";
            $validation_ok = false;
        }

        $json = file_get_contents('https://geoip-db.com/json');
        $data = json_decode($json);

        $country = $data->country_name;

        if($validation_ok)
        {
            $result = $user->addUser($username, $password1, $email, $country);

            if($result)
            {
                $_SESSION['success'] = 'Account successfully created!</a>';
            }
            else
            {
                $_SESSION['server_error'] = 'Server error!';
            }
        }
    }

?>
<!DOCTYPE HTML>

<html lang="pl">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome-1">

    <title>Login System</title>

    <link rel="stylesheet" href="style/main.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700&amp;subset=latin-ext" rel="stylesheet">
    <link rel="stylesheet" href="style/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="style/fontello/css/fontello.css">

    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script src="style/bootstrap/jquery-3.2.1.min.js"></script>
    <script src="style/bootstrap/js/bootstrap.js"></script>

</head>

<body>

  <nav class="navbar navbar-inverse navbar-static-top">

    <div class="container-fluid">

      <div class="navbar-header">
        <a class="navbar-brand" href="index.php"><i class="icon-code"></i>Login<span style="font-weight: 300;">System</span></a>
      </div>

      <ul class="nav navbar-nav navbar-right">

        <li><a href="index.php"><i class="icon-login"></i>Sign in</a></li>
        <li class="active"><a href="register.php"><i class="icon-key"></i>Sign Up</a></li>

      </ul>
    </div>

  </nav>

  <article>

    <div class="wrapper">

      <div class="panel panel-default">

        <div class="panel-heading"><i class="icon-key"></i>Sign up</div>

        <div class="panel-body">

            <form method="post">

              <input type="text" class="form-control" placeholder="&#xe806;&nbsp;Username" name="username">
              <input type="email" class="form-control" placeholder="&#xf0e0;&nbsp;Email" name="email">
              <input type="password" class="form-control" placeholder="&#xe802;&nbsp;Password" name="password1">
              <input type="password" class="form-control" placeholder="&#xe802;&nbsp;Repeat Password" name="password2">

              <label> <input type="checkbox" name="terms-of-service"> Terms of service</label>

              <div class="g-recaptcha" data-sitekey="6Lc2VCcUAAAAAJy6HxKkdrwwcKkNg_COukU7vSii"></div>

              <div style="height: 20px;"></div>

              <input type="submit" value="Sign up" class="btn btn-primary"> Do you have an account? <a href="index.php">Sign in.</a>
            </form>

        </div>

      </div>

        <?php
            if(isset($_SESSION['success']))
            {
                echo '<div class="alert alert-success alert-dismissable fade in">';
                echo '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
                echo  '<i class="icon-ok"></i>'.$_SESSION['success'];
                echo '</div>';

                unset($_SESSION['success']);
            }

            if(isset($_SESSION['e_username']))
            {
                echo '<div class="alert alert-danger alert-dismissable fade in">';
                echo '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
                echo '<i class="icon-attention"></i>'.$_SESSION['e_username'];
                echo '</div>';

                unset($_SESSION['e_username']);
            }

            if(isset($_SESSION['e_email']))
            {
                echo '<div class="alert alert-danger alert-dismissable fade in">';
                echo '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
                echo '<i class="icon-attention"></i>'.$_SESSION['e_email'];
                echo '</div>';

                unset($_SESSION['e_email']);
            }

            if(isset($_SESSION['e_pass']))
            {
                echo '<div class="alert alert-danger alert-dismissable fade in">';
                echo '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
                echo  '<i class="icon-attention"></i>'.$_SESSION['e_pass'];
                echo '</div>';

                unset($_SESSION['e_pass']);
            }

            if(isset($_SESSION['e_checkbox']))
            {
                echo '<div class="alert alert-danger alert-dismissable fade in">';
                echo '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
                echo  '<i class="icon-attention"></i>'.$_SESSION['e_checkbox'];
                echo '</div>';

                unset($_SESSION['e_checkbox']);
            }

            if(isset($_SESSION['e_recaptcha']))
            {
                echo '<div class="alert alert-danger alert-dismissable fade in">';
                echo '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
                echo  '<i class="icon-attention"></i>'.$_SESSION['e_recaptcha'];
                echo '</div>';

                unset($_SESSION['e_recaptcha']);
            }

            if(isset($_SESSION['server_error']))
            {
                echo '<div class="alert alert-danger alert-dismissable fade in">';
                echo '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
                echo  '<i class="icon-server"></i>'.$_SESSION['server_error'];
                echo '</div>';

                unset($_SESSION['server_error']);
            }

        ?>

    </div>

  </article>

</body>

</html>
