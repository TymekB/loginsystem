<?php

    require "User.php";

    if(!isset($_POST['username']))
    {
        header('Location: index.php');
    }
    else
    {
        $username = $_POST['username'];
        $password = $_POST['password'];

        $user = new User();

        $result = $user->searchByUserAndPass($username, $password);

        if($result)
        {
            $user->logIn($result);
            header('Location: ../panel.php');
        }
        else
        {
            $_SESSION['error'] = "Invalid username or password!";
            header('Location: ../index.php');
        }
    }
