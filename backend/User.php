<?php

class User
{
    private $mysql;

    function __construct()
    {
        require "config.php";

        $this->mysql = new mysqli($host, $db_user, $db_password, $db_name);

        if (session_status() == PHP_SESSION_NONE)
        {
            session_start();
        }
    }

    public function searchByUserAndPass($username, $password)
    {
        $userInfo = array();
        $username = htmlentities($username, ENT_QUOTES);

        try
        {
            if($this->mysql->connect_errno != 0) throw new Exception(mysqli_connect_error());
            else
            {
                $sql = sprintf("SELECT * FROM users WHERE username='%s'", mysqli_real_escape_string($this->mysql, $username));

                $result = $this->mysql->query($sql);

                if(!$result) throw new Exception($this->mysql->error);
                else
                {
                    if($result->num_rows == 1)
                    {
                        $row = $result->fetch_assoc();

                        if(password_verify($password, $row['password']))
                        {
                            $userInfo = $row;
                        }
                    }
                }

            }
        }
        catch(Exception $e)
        {
            $file = fopen('errors.txt', 'a');

            fwrite($file, $e);
        }

        return $userInfo;
    }

    public function logIn($userInfo)
    {
        $_SESSION['logged'] = true;
        $_SESSION['userInfo'] = $userInfo;
    }

    public function isLogged()
    {
        if(isset($_SESSION['logged']) && $_SESSION['logged'] == true)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function logOut()
    {
        if(session_status() == PHP_SESSION_ACTIVE)
        {
            session_unset();
            return true;
        }
        else
        {
            return false;
        }
    }

    public function updateUsername($username, $id)
    {
        $updated = false;
        $username = htmlentities($username, ENT_QUOTES);

        try
        {
            if($this->mysql->connect_errno != 0) throw new Exception(mysqli_connect_error());
            else
            {
                $sql = sprintf("UPDATE users SET username='%s' WHERE id='$id'", mysqli_real_escape_string($this->mysql, $username));

                $result = $this->mysql->query($sql);

                if(!$result) throw new Exception($this->mysql->error);
                else
                {
                    $updated = true;
                }
            }
        }
        catch(Exception $e)
        {
            $file = fopen('errors.txt', 'a');

            fwrite($file, $e);
        }

        return $updated;
    }

    public function updateEmail($email, $id)
    {
        $updated = false;

        try
        {
            if($this->mysql->connect_errno != 0) throw new Exception(mysqli_connect_error());
            else
            {
                $sql = sprintf("UPDATE users SET email='%s' WHERE id='$id'" ,mysqli_real_escape_string($this->mysql, $email));

                $result = $this->mysql->query($sql);

                if(!$result) throw new Exception($this->mysql->error);
                {
                    $updated = true;
                }
            }
        }
        catch(Exception $e)
        {
            $file = fopen('errors.txt', 'a');

            fwrite($file, $e);
        }

        return $updated;
    }

    public function updatePassword($password, $id)
    {
        $updated = false;

        try
        {
            if($this->mysql->connect_errno != 0) throw new Exception(mysqli_connect_error());
            else
            {
                $password_hash = password_hash($password, PASSWORD_DEFAULT);

                $sql = "UPDATE users SET password='$password_hash' WHERE id='$id'";

                $result = $this->mysql->query($sql);

                if(!$result) throw new Exception($this->mysql->error);
                else
                {
                    $updated = true;
                }
            }
        }
        catch(Exception $e)
        {
            $file = fopen('errors.txt', 'a');

            fwrite($file, $e);
        }

        return $updated;
    }

    public function searchUsername($username)
    {
        $usernameFinded = false;

        try
        {
            if($this->mysql->connect_errno != 0) throw new Exception(mysqli_connect_error());
            else
            {
                $sql = sprintf("SELECT * FROM users WHERE username='%s'", mysqli_real_escape_string($this->mysql, $username));

                $result = $this->mysql->query($sql);

                if(!$result) throw new Exception($this->mysql->error);
                else
                {
                    if($result->num_rows == 1)
                    {
                        $usernameFinded = true;
                    }
                }
            }
        }
        catch(Exception $e)
        {
            $file = fopen('errors.txt', 'a');

            fwrite($file, $e);
        }

        return $usernameFinded;
    }

    public function searchEmail($email)
    {
        $emailFinded = false;

        try
        {
            if($this->mysql->connect_errno != 0) throw new Exception(mysqli_connect_error());
            else
            {
                $sql = sprintf("SELECT * FROM users WHERE email='%s'", mysqli_real_escape_string($this->mysql, $email));

                $result = $this->mysql->query($sql);

                if(!$result) throw new Exception($this->mysql->error);
                else
                {
                    if($result->num_rows == 1)
                    {
                        $emailFinded = true;
                    }
                }
            }
        }
        catch(Exception $e)
        {
            $file = fopen('errors.txt', 'a');

            fwrite($file, $e);
        }

        return $emailFinded;
    }

    public function addUser($username, $password, $email, $country)
    {
        $userAdded = false;

        try
        {
            if($this->mysql->connect_errno != 0) throw new Exception(mysqli_connect_error());
            else
            {
                $password_hash = password_hash($password, PASSWORD_DEFAULT);

                $sql = sprintf("INSERT INTO users VALUES(null, '%s', '$password_hash', '%s', NOW(), '%s')",
                    mysqli_real_escape_string($this->mysql, $username),
                    mysqli_real_escape_string($this->mysql, $email),
                    mysqli_real_escape_string($this->mysql, $country));

                $result = $this->mysql->query($sql);

                if(!$result) throw new Exception($this->mysql->error);
                else
                {
                    $userAdded = true;
                }
            }
        }
        catch(Exception $e)
        {
            $file = fopen('errors.txt', 'a');

            fwrite($file, $e);
        }

        return $userAdded;
    }

}