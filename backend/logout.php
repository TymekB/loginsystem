<?php

    require "User.php";

    $user = new User();

    if($user->logOut())
    {
        header('Location: ../index.php');
    }
    else
    {
        header('Location: ../panel.php');
    }