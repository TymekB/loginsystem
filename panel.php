<?php
    require "backend/User.php";

    $user = new User();

    if(!$user->isLogged())
    {
        header('Location: index.php');
        exit();
    }
?>
<!DOCTYPE HTML>

<html lang="pl">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome-1">

    <title>Login System</title>

    <link rel="stylesheet" href="style/main.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700&amp;subset=latin-ext" rel="stylesheet">
    <link rel="stylesheet" href="style/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="style/fontello/css/fontello.css">

    <script src="style/bootstrap/jquery-3.2.1.min.js"></script>
    <script src="style/bootstrap/js/bootstrap.js"></script>

</head>

<body>

<nav class="navbar navbar-inverse navbar-static-top">

    <div class="container-fluid">

        <div class="navbar-header">
            <a class="navbar-brand" href="index.php"><i class="icon-code"></i>Login<span style="font-weight: 300;">System</span></a>
        </div>

        <ul class="nav navbar-nav navbar-right">

            <li class="dropdown">

                <a class="dropdown-toggle" data-toggle="dropdown" href="#"><?php echo $_SESSION['userInfo']['username']; ?>

                <span class="caret"></span></a>

                <ul class="dropdown-menu">

                    <li><a href="panel.php"><i class="icon-user"></i>My account</a></li>
                    <li><a href="settings.php"><i class="icon-cog"></i>Settings</a></li>
                    <li><a href="backend/logout.php"><i class="icon-logout"></i>Logout</a></li>
                </ul>

            </li>

        </ul>
    </div>

</nav>

<article>

    <div class="wrapper">

        <div class="panel panel-default">

            <div class="panel-heading"><i class="icon-user"></i> My Account</div>

            <div class="panel-body">
                <span style="font-size: 17px">
                    <?php

                        echo '<i class="icon-user-1"></i>Username: <span style="font-weight: 300">'.$_SESSION['userInfo']['username'].'</span><br>';
                        echo '<i class="icon-mail-alt"></i>Email: <span style="font-weight: 300">'.$_SESSION['userInfo']['email'].'</span><br>';
                        echo '<i class="icon-calendar-empty"></i>Creation date: <span style="font-weight: 300">'.$_SESSION['userInfo']['date'].'</span><br>';
                        echo '<i class="icon-location"></i>Country: <span style="font-weight: 300">'.$_SESSION['userInfo']['country'].'</span>';
                    ?>
                </span>
            </div>

        </div>

    </div>

</article>

</body>

</html>
