<?php
    require "backend/User.php";

    $user = new User();

    if(!$user->isLogged())
    {
        header('Location: index.php');
        exit();
    }

    if(isset($_POST['username']))
    {
        $validation_ok = true;
        $username = $_POST['username'];
        $id = $_SESSION['userInfo']['id'];

        if(strlen($username) < 3)
        {
            $_SESSION['e_username'] = "Username is too short! (3-20)";
            $validation_ok = false;
        }

        if(strlen($username) > 20)
        {
            $_SESSION['e_username'] = "Username is too long! (3-20)";
            $validation_ok = false;
        }

        if(!ctype_alnum($username))
        {
            $_SESSION['e_username'] = "Username is invalid";
            $validation_ok = false;
        }

        if($user->searchUsername($username))
        {
            $_SESSION['e_username'] = "Username already exist!";
            $validation_ok = false;
        }

        if($validation_ok)
        {
            $result = $user->updateUsername($username, $id);

            if($result)
            {
                $_SESSION['userInfo']['username'] = $username;
                $_SESSION['success_username'] = 'Username updated successfully!';
            }
            else
            {
                $_SESSION['server_error'] = 'Server error!';
            }
        }
    }

    if(isset($_POST['email']))
    {
        $validation_ok = true;
        $email = $_POST['email'];
        $id = $_SESSION['userInfo']['id'];

        $sanitizedEmail = filter_var($email, FILTER_SANITIZE_EMAIL);

        if(!filter_var($sanitizedEmail, FILTER_VALIDATE_EMAIL) || $sanitizedEmail != $email)
        {
            $_SESSION['e_email'] = "Email is invalid!";
            $validation_ok = false;
        }

        if($user->searchEmail($email))
        {
            $_SESSION['e_email'] = "Email already exist!";
            $validation_ok = false;
        }

        if($validation_ok)
        {
            $result = $user->updateEmail($email, $id);

            if($result)
            {
                $_SESSION['userInfo']['email'] = $email;
                $_SESSION['success_email'] = 'Email updated successfully!';
            }
            else
            {
                $_SESSION['server_error'] = 'Server error!';
            }
        }
    }

    if(isset($_POST['newpassword']) && !empty($_POST['newpassword']))
    {
        $validation_ok = true;
        $newPassword = $_POST['newpassword'];
        $newPassword2 = $_POST['newpassword2'];
        $id = $_SESSION['userInfo']['id'];

        if(strlen($newPassword) < 8)
        {
            $_SESSION['e_pass'] = "Password is too short! (8-20)";
            $validation_ok = false;
        }

        if(strlen($newPassword) > 20)
        {
            $_SESSION['e_pass'] = "Password is too long! (8-20)";
            $validation_ok = false;
        }

        if($newPassword != $newPassword2)
        {
            $_SESSION['e_pass'] = "Passwords are not the same!";
            $validation_ok = false;
        }

        if($validation_ok)
        {
            $result = $user->updatePassword($newPassword, $id);

            if($result)
            {
                $_SESSION['success_pass'] = 'Password updated successfully!';
            }
            else
            {
                $_SESSION['server_error'] = 'Server error!';
            }
        }
    }

?>
<!DOCTYPE HTML>

<html lang="pl">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome-1">

    <title>Login System</title>

    <link rel="stylesheet" href="style/main.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700&amp;subset=latin-ext" rel="stylesheet">
    <link rel="stylesheet" href="style/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="style/fontello/css/fontello.css">

    <script src="style/bootstrap/jquery-3.2.1.min.js"></script>
    <script src="style/bootstrap/js/bootstrap.js"></script>

</head>

<body>

<nav class="navbar navbar-inverse navbar-static-top">

    <div class="container-fluid">

        <div class="navbar-header">
            <a class="navbar-brand" href="index.php"><i class="icon-code"></i>Login<span style="font-weight: 300;">System</span></a>
        </div>

        <ul class="nav navbar-nav navbar-right">

            <li class="dropdown">

                <a class="dropdown-toggle" data-toggle="dropdown" href="#"><?php echo $_SESSION['userInfo']['username']; ?>

                    <span class="caret"></span></a>

                <ul class="dropdown-menu">

                    <li><a href="panel.php"><i class="icon-user"></i>My account</a></li>
                    <li><a href="settings.php"><i class="icon-cog"></i>Settings</a></li>
                    <li><a href="backend/logout.php"><i class="icon-logout"></i>Logout</a></li>
                </ul>

            </li>

        </ul>
    </div>

</nav>

<article>

    <div class="wrapper">

        <div class="panel panel-default">

            <div class="panel-heading"><i class="icon-cog"></i> Settings</div>

            <div class="panel-body">

                <form method="post">

                    <label>Username</label> <br>
                    <input type="text" value="<?php echo $_SESSION['userInfo']['username']; ?>" class="form-control" style="width: 225px; float: left;" name="username">
                    <input type="submit" value="update" class="btn btn-success" style="float: right;">

                </form>

                <div style="clear: both;"></div>

                <form method="post">

                    <label>Email</label> <br>
                    <input type="email" value="<?php echo $_SESSION['userInfo']['email']; ?>" class="form-control" style="width: 225px; float: left;" name="email">
                    <input type="submit" value="update" class="btn btn-success" style="float: right;">

                </form>

                <div style="clear: both"></div>

                <label>Creation date</label> <br>
                <input type="date" value="<?php echo $_SESSION['userInfo']['date']; ?>" class="form-control" style="width: 225px; float: left;" disabled>
                <input type="submit" value="update" class="btn btn-success" style="float: right;" disabled>

                <div style="clear: both;"></div>

                <label>Country</label> <br>
                <input type="text" value="<?php echo $_SESSION['userInfo']['country']; ?>" class="form-control" style="width: 225px; float: left;" disabled>
                <input type="submit" value="update" class="btn btn-success" style="float: right;" disabled>

                <div style="clear: both"></div>

                <form method="post">

                    <label>New password</label> <br>
                    <input type="password" placeholder="********" class="form-control" name="newpassword">

                    <label>Repeat new password</label> <br>
                    <input type="password" placeholder="********" class="form-control" name="newpassword2">

                    <input type="submit" value="Update password" class="btn btn-primary">

                </form>

            </div>

        </div>

        <?php
            // Success

            if(isset($_SESSION['success_username']))
            {
                echo '<div class="alert alert-success alert-dismissable fade in">';
                echo '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
                echo  '<i class="icon-ok"></i>'.$_SESSION['success_username'];
                echo '</div>';

                unset($_SESSION['success_username']);
            }

            if(isset($_SESSION['success_email']))
            {
                echo '<div class="alert alert-success alert-dismissable fade in">';
                echo '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
                echo  '<i class="icon-ok"></i>'.$_SESSION['success_email'];
                echo '</div>';

                unset($_SESSION['success_email']);
            }

            if(isset($_SESSION['success_pass']))
            {
                echo '<div class="alert alert-success alert-dismissable fade in">';
                echo '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
                echo  '<i class="icon-ok"></i>'.$_SESSION['success_pass'];
                echo '</div>';

                unset($_SESSION['success_pass']);
            }

            // Errors

            if(isset($_SESSION['e_username']))
            {
                echo '<div class="alert alert-danger alert-dismissable fade in">';
                echo '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
                echo '<i class="icon-attention"></i>'.$_SESSION['e_username'];
                echo '</div>';

                unset($_SESSION['e_username']);
            }

            if(isset($_SESSION['e_email']))
            {
                echo '<div class="alert alert-danger alert-dismissable fade in">';
                echo '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
                echo '<i class="icon-attention"></i>'.$_SESSION['e_email'];
                echo '</div>';

                unset($_SESSION['e_email']);
            }

            if(isset($_SESSION['e_pass']))
            {
                echo '<div class="alert alert-danger alert-dismissable fade in">';
                echo '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
                echo  '<i class="icon-attention"></i>'.$_SESSION['e_pass'];
                echo '</div>';

                unset($_SESSION['e_pass']);
            }

            if(isset($_SESSION['server_error']))
            {
                echo '<div class="alert alert-danger alert-dismissable fade in">';
                echo '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
                echo  '<i class="icon-server"></i>'.$_SESSION['server_error'];
                echo '</div>';

                unset($_SESSION['server_error']);
            }


        ?>

    </div>

</article>

</body>

</html>
