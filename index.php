<?php

    require "backend/User.php";

    $user = new User();

    if($user->isLogged())
    {
        header("Location: panel.php");
        exit();
    }
?>
<!DOCTYPE HTML>

<html lang="pl">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome-1">

    <title>Login System</title>

    <link rel="stylesheet" href="style/main.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700&amp;subset=latin-ext" rel="stylesheet">
    <link rel="stylesheet" href="style/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="style/fontello/css/fontello.css">

    <script src="style/bootstrap/jquery-3.2.1.min.js"></script>
    <script src="style/bootstrap/js/bootstrap.js"></script>

</head>

<body>

  <nav class="navbar navbar-inverse navbar-static-top">

    <div class="container-fluid">

      <div class="navbar-header">
        <a class="navbar-brand" href="index.php"><i class="icon-code"></i>Login<span style="font-weight: 300;">System</span></a>
      </div>

      <ul class="nav navbar-nav navbar-right">

        <li class="active"><a href="index.php"><i class="icon-login"></i>Sign in</a></li>
        <li><a href="register.php"><i class="icon-key"></i>Sign Up</a></li>

      </ul>
    </div>

  </nav>

  <article>

    <div class="wrapper">

        <div class="panel panel-default">

        <div class="panel-heading"><i class="icon-login"></i>Sign in</div>

        <div class="panel-body">

            <form action="backend/login.php" method="post">

              <input type="text" class="form-control" placeholder="&#xe806;&nbsp;Username" name="username">
              <input type="password" class="form-control" placeholder="&#xe802;&nbsp;Password" name="password">

              <div style="height: 20px;"></div>

              <input type="submit" value="Log in" class="btn btn-primary"> Are you new here? <a href="register.php">Sign up.</a>

            </form>

        </div>

        </div>

        <?php
            if(isset($_SESSION['error']))
            {
                echo '<div class="alert alert-danger alert-dismissable fade in">';
                echo '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
                echo  '<i class="icon-attention"></i>'.$_SESSION['error'];
                echo '</div>';

                unset($_SESSION['error']);
            }

        ?>

    </div>

  </article>

</body>

</html>
